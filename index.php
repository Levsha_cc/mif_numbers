<?php

function check($a) {
    $curr = $a[0];
    foreach ($a as $e) {
        if ($e >= $curr) {
            $curr = $e;
        }
        else {
            return false;
        }
    }
    return true;
}

$n = rand(2, 10);
$numbers = [];
for ($i = 0; $i < $n; $i++) {
    $numbers[] = rand(1, 100000);
}

echo $n;
echo "\r\n<br/>";
echo implode(', ', $numbers);
echo "\r\n<br/>";

for ($i=0; $i<$n; $i++) {
    if (check($numbers)) {
        die($i);
    }
    else {
        array_unshift($numbers, array_pop($numbers));
    }
}

echo "-1";
